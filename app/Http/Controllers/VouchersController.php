<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;
use App\VouchersModel;

class VouchersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	private $request;
	private $voucherList = [];
	private $codeLength = 10;
	private $session = "";
	
    public function __construct()
    {
    }

    //
	
	public function generate(Request $request) {
	    $validatedData = $request->validate([
			'number' => 'bail|required|integer|max:1000000',
		]);	

        $this->session = "s_".time();	
		$time_start = microtime(true); 
		$postData = $request->all();
		if(empty($postData['number'])) return json_encode(array('error' => 'please enter number'));
		$this->codeLength = strlen ((string) $postData['number']);
		if($this->codeLength < 6) $this->codeLength = 6;
		
		$this->generator($postData['number']);
		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start);
		$result_array = array('session_id' => $this->session, "execution_time" => $execution_time);
		return view('results', ['execution_time' => $execution_time , 'url' => './codes/'. $this->session ]);

		
		//return $result_array;		
		
		
	
	}


	private function generator($number) {
		
		while (sizeof($this->voucherList) < ($number * 1.1)) {
			$this->voucherList[] = $this->codeGenerator();
		}
		
		$this->voucherList = array_keys(array_flip($this->voucherList));
		$this->voucherList = array_slice($this->voucherList , 0, $number);
		
		$insertList = [];	
		foreach ($this->voucherList AS $v) {
			$insertList[] = array("session" => $this->session, "voucher_code" => $v);			
		}
		$this->saveVouchers( $this->session, $insertList);
		
		}
	
	private function codeGenerator() {
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "0123456789";
		$max = strlen($codeAlphabet);

		for ($i=0; $i < $this->codeLength; $i++) {
			$token .= $codeAlphabet[random_int(0, $max-1)];
		}
		return $token;
	}	
	
	private function saveVouchers($session, $insertList, $batch = 20000) {	
		$tempList = [];
		for ($i = 0 ; $i < ceil(sizeof($insertList) / $batch); $i++) {
			$tempList = array_slice($insertList , ($i * $batch), $batch);
			VouchersModel::insert($tempList);
		}
	}
		
	private function validateExist($session, $voucher_code) {
		//return true;
		if(in_array($voucher_code,$this->voucherList)) return true;
		return false;
	}
	
	public function getList(Request $request, $session) {
		$codes = VouchersModel::where('session', $session)->get();
		$results = [];
		foreach ($codes AS $v) {
			$results[] = $v->voucher_code;
		}
		
		//$results = json_decode($response->body(),true);	
		
		//return view('codes', ['results' => $response->body()]);
		return view('codes', ['vouchers' => $results]);		
		//return json_encode($results);
		
		
	}

	/*
	public function getList(Request $request, $session) {
		
		$postData = $request->all();

		$response = Http::get('http://backend.reiner.local/codes/'.$session, [
		]);

				
	}*/
	
}
